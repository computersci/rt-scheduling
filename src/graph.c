#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void write_tikz_preamble(FILE *fp) {
    fprintf(fp, "\\begin{tikzpicture}\n"
        "\t\\begin{axis}[\n"
        "\t\txmin=0,xlabel=Time,\n"
        "\t\tylabel=Task,ytick distance=1,enlarge y limits=0.8,\n"
        "\t\tmark size=3,\n"
        "\t\tscatter/classes={\n"
        "\t\t\tmiss={mark=diamond,draw=red},\n"
        "\t\t\tcomplete={mark=triangle,draw=green}}]\n\n");
}

void write_tikz_postamble(FILE *fp) {
    fprintf(fp, "\t\\end{axis}\n\\end{tikzpicture}");
}

int main(int argc, char *argv[]) {
    for (int i = 1; i < argc; i++) {
        printf("Producing graph for %s...\n", argv[i]);
        fflush(stdout);
        FILE *fp = fopen(argv[i], "r");
        char *path = (char*) malloc(strlen(argv[i]) + 4);
        sprintf(path, "%s.tex", argv[i]);
        FILE *out = fopen(path, "w");
        write_tikz_preamble(out);

        // Pass 1: Write all misses and completions
        printf("Pass 1: Misses and Completions\n", argv[i]);
        fflush(stdout);
        fprintf(out, "\t\t\t\\addplot[scatter,only marks,"
            "scatter src=explicit symbolic,line width=2pt]\n"
            "\t\t\t\ttable[meta=label] {\n"
            "\t\t\t\t\tx y label\n");

        int res;
        int time;
        int task;
        char buf[255];
        char action[10];
        while (fgets(buf, 254, fp)) {
            if (strcmp(buf, "Scheduling Finishes\n") == 0) break;
            sscanf(buf, "%d Task %d %s\n", &time, &task, action);
            if (strcmp(action, "Completes") == 0) {
                fprintf(out, "\t\t\t\t\t%d %d %s\n", time + 1, task, "complete");
            } else if (strcmp(action, "Misses") == 0) {
                fprintf(out, "\t\t\t\t\t%d %d %s\n", time, task, "miss");
            }
        }
        fprintf(out, "\t\t\t\t};\n");

        // Pass 2: Write all task changes
        printf("Pass 2: Task Changes\n", argv[i]);
        fflush(stdout);
        rewind(fp);
        fprintf(out, "\t\t\t\\addplot+[const plot,jump mark left,mark=none,line width=10pt,color=blue] coordinates {");
        unsigned int last_task = -1;
        while (fgets(buf, 254, fp)) {
            if (strcmp(buf, "Scheduling Finishes\n") == 0) break;
            sscanf(buf, "%d Task %d %s\n", &time, &task, action);
            if (strcmp(action, "Executes") == 0) {
                if (task != last_task) {
                    last_task = task;
                    fprintf(out, "(%d,%d) ", time, task);
                }
            }
            if (fscanf(fp, "Scheduling Finishes\n")) break;
        }
        fprintf(out, "};\n");

        write_tikz_postamble(out);
        fclose(out);
        fclose(fp);
    }
    return 0;
}
