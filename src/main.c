#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct _task
{
    /// The task identifier, read from the input file.
    unsigned int ident;
    /// The execution time of the task, read from the input file.
    unsigned int execution_time;
    /// The period of the task, read from the input file.
    unsigned int period;
} task;

typedef struct _task_instance
{
    /// Is this task already completed?
    bool complete;
    /// The index in the loaded task array of the task this instance references.
    unsigned int task_index;
    /// The time remaining to complete execution.
    unsigned int execution_time_left;
    /// The deadline time of this task.
    unsigned int deadline;
} task_instance;

typedef struct _task_list
{
    unsigned int count;
    unsigned int superperiod;
    task tasks[];
} task_list;

typedef struct _simulation_state
{
    FILE *fp;
    unsigned int num_deadline_misses;
    unsigned int num_instances;
    task_instance instances[];
} simulation_state;

simulation_state* new_simulation_state(unsigned int instance_capacity) {
    simulation_state *p = (simulation_state*) malloc(sizeof(simulation_state) + sizeof(task_instance) * instance_capacity);
    p->fp = NULL;
    p->num_deadline_misses = 0;
    p->num_instances = 0;
    return p;
}

int gcd(int a, int b) {
    if (a > b) return gcd(b, a);
    if (b % a == 0) return a;
    return gcd(b % a, a);
}

int lcm(int a, int b) {
    return (a * b) / gcd(a, b);
}

/// Calculate the superperiod over a set of tasks.
int calculate_superperiod(task_list *tasks) {
    // Find the lowest common multiple of the periods of all tasks.
    unsigned int s = 1;
    for (int i = 0; i < tasks->count; i++) {
        s = lcm(s, tasks->tasks[i].period);
    }
    return s;
}

/// Loads tasks from a given file, returning the number of tasks
/// loaded.
void load_tasks(FILE *file, task_list **tasks) {
    // Read number of tasks from file
    int taskc = 0;
    fscanf(file, "NumTasks %d\n", &taskc);
    *tasks = (task_list*) malloc(sizeof(task_list) + sizeof(task) * taskc);
    task_list *_tasks = *tasks;
    _tasks->count = taskc;
    printf("Expecting %d tasks...\n", taskc);

    // Load each task.
    for (int i = 0; i < taskc; i++) {
        unsigned int ident;
        unsigned int execution_time;
        unsigned int period;
        if (fscanf(file, "Task%d %d %d\n", &ident, &execution_time, &period) != 3)
            continue;
        
        _tasks->tasks[i].ident = ident;
        _tasks->tasks[i].execution_time = execution_time;
        _tasks->tasks[i].period = period;
        printf("Found task `%d' (%d, %d)\n",
            _tasks->tasks[i].ident,
            _tasks->tasks[i].execution_time,
            _tasks->tasks[i].period);
    }
}

/// Find task with shortest period
int rms(simulation_state *state, task_list *tasks) {
    int executing_task_inst_index = 0;
    unsigned int current_shortest = -1;
    for (int in = 0; in < state->num_instances; in++) {
        if (state->instances[in].complete) continue;
        unsigned int period = tasks->tasks[state->instances[in].task_index].period;
        if (period < current_shortest) {
            current_shortest = period;
            executing_task_inst_index = in;
        }
    }
    return executing_task_inst_index;
}

// Find earliest deadline
int edf(simulation_state *state, task_list *tasks) {
    int executing_task_inst_index = 0;
    unsigned int current_earliest = -1;
    for (int in = 0; in < state->num_instances; in++) {
        if (state->instances[in].complete) continue;
        if (state->instances[in].deadline < current_earliest) {
            current_earliest = state->instances[in].deadline;
            executing_task_inst_index = in;
        }
    }
    return executing_task_inst_index;
}

int main(int argc, char *argv[]) {
    for (int i = 1; i < argc; i++) {
        // Load tasks
        printf("Loading tasks from %s...\n", argv[i]);
        task_list *tasks; // Array of pointers to task
        FILE *task_file = fopen(argv[i], "r");
        load_tasks(task_file, &tasks);
        
        tasks->superperiod = calculate_superperiod(tasks);

        // Simulate
        printf("Simulating over the %ds superperiod...\n", tasks->superperiod);
        fflush(stdout);

        simulation_state *edf_state = new_simulation_state(tasks->superperiod * tasks->count);
        simulation_state *rms_state = new_simulation_state(tasks->superperiod * tasks->count);

        char edf_output_path[255];
        sprintf(edf_output_path, "%s-edf.out", argv[i]);
        char rms_output_path[255];
        sprintf(rms_output_path, "%s-rms.out", argv[i]);
        edf_state->fp = fopen(edf_output_path, "w");
        rms_state->fp = fopen(rms_output_path, "w");

        for (int t = 0; t < tasks->superperiod; t++) {
            // Check for any new tasks requiring scheduling
            for (int j = 0; j < tasks->count; j++) {
                if (t % tasks->tasks[j].period == 0) {
                    // Append new task instance EDF
                    edf_state->instances[edf_state->num_instances].complete = false;
                    edf_state->instances[edf_state->num_instances].task_index = j;
                    edf_state->instances[edf_state->num_instances].deadline = t + tasks->tasks[j].period;
                    edf_state->instances[edf_state->num_instances++].execution_time_left = tasks->tasks[j].execution_time;
                    // Append new task instance RMS
                    rms_state->instances[rms_state->num_instances].complete = false;
                    rms_state->instances[rms_state->num_instances].task_index = j;
                    rms_state->instances[rms_state->num_instances].deadline = t + tasks->tasks[j].period;
                    rms_state->instances[rms_state->num_instances++].execution_time_left = tasks->tasks[j].execution_time;
                }
            }

            // Determine misses EDF
            for (int in = 0; in < edf_state->num_instances; in++) {
                if (edf_state->instances[in].complete) continue;
                if (edf_state->instances[in].deadline < t) {
                    fprintf(edf_state->fp, "%d Task %d Misses\n", t, tasks->tasks[edf_state->instances[in].task_index].ident);
                    edf_state->num_deadline_misses++;
                }
            }
            // Determine misses RMS
            for (int in = 0; in < rms_state->num_instances; in++) {
                if (rms_state->instances[in].complete) continue;
                if (rms_state->instances[in].deadline < t) {
                    fprintf(rms_state->fp, "%d Task %d Misses\n", t, tasks->tasks[rms_state->instances[in].task_index].ident);
                    rms_state->num_deadline_misses++;
                }
            }

            // Determine executes EDF
            int executing_task_inst_index = edf(edf_state, tasks);
            int executing_task_index = edf_state->instances[executing_task_inst_index].task_index;
            fprintf(edf_state->fp, "%d Task %d Executes\n", t, tasks->tasks[executing_task_index].ident);
            if (--edf_state->instances[executing_task_inst_index].execution_time_left == 0) {
                // Completed
                fprintf(edf_state->fp, "%d Task %d Completes\n", t, tasks->tasks[executing_task_index].ident);
                edf_state->instances[executing_task_inst_index].complete = true;
            }
            // Determine executes RMS
            executing_task_inst_index = rms(rms_state, tasks);
            executing_task_index = rms_state->instances[executing_task_inst_index].task_index;
            fprintf(rms_state->fp, "%d Task %d Executes\n", t, tasks->tasks[executing_task_index].ident);
            if (--rms_state->instances[executing_task_inst_index].execution_time_left == 0) {
                // Completed
                fprintf(rms_state->fp, "%d Task %d Completes\n", t, tasks->tasks[executing_task_index].ident);
                rms_state->instances[executing_task_inst_index].complete = true;
            }
        }

        fprintf(edf_state->fp, "Scheduling Finishes\n%d misses\n", edf_state->num_deadline_misses);
        fprintf(rms_state->fp, "Scheduling Finishes\n%d misses\n", rms_state->num_deadline_misses);
        
        // Cleanup
        fclose(edf_state->fp);
        fclose(rms_state->fp);
        fclose(task_file);
        free(tasks);
    }

    return 0;
}
