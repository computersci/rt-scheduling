CFLAGS=-std=c99 -pedantic -g
BUILD_DIR?=./build

$(BUILD_DIR)/rt-scheduling: src/main.c
	mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) -o $@ $<

$(BUILD_DIR)/graphgen: src/graph.c
	mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) -o $@ $<

report.pdf: run report.tex
	pdflatex report.tex

.PHONY: clean run
clean:
	rm -r $(BUILD_DIR)
run: $(BUILD_DIR)/rt-scheduling $(BUILD_DIR)/graphgen
	$(BUILD_DIR)/rt-scheduling tests/1.txt tests/2.txt tests/3.txt tests/4.txt
	$(BUILD_DIR)/graphgen tests/1.txt-edf.out tests/1.txt-rms.out tests/2.txt-edf.out tests/2.txt-rms.out tests/3.txt-edf.out tests/3.txt-rms.out tests/4.txt-edf.out tests/4.txt-rms.out
